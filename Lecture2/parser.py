'''
Created on Oct 16, 2012

@author: weralwolf
'''

class DataRow:
    def __init__(self):
        self.orbit = 0;
        self.year = 1984;
        self.day_of_year = 0;
        self.ut = 0;
        self.temp = 0.;
        self.np = 0.;
        self.potential = 0.;
        self.altitude = 0.;
        self.latitude = 0.;
        self.longitude = 0.;
        self.lst = 0.;
        self.lmt = 0.;
        self.l_sh = 0.;
        self.inv_lat = 0.;
        self.sza = 0.;
    
    def __repr__(self):
        return "<lang_500ms_v01: y: %i, d: %i, ut: %i>" % (self.year, self.day_of_year, self.ut);

def parser(filename):
    # reading data from current file
    data = open(filename, 'r').readlines();
    
    # removing 3 descriptive lines in the begin of file
    data.pop(0); data.pop(0); data.pop(0);
    
    rows = [];
    
    for i in data:
        row = DataRow();
        row.orbit = i[0:6];
        row.year = '19' + i[7:9];
        row.day_of_year = i[9:12];
        row.ut = i[12:21];
        row.temp = i[21:28];
        row.np = i[28:38];
        row.potential = i[38:45];
        row.altitude = i[45:53];
        row.latitude = i[53:61];
        row.longitude = i[61:69];
        row.lst = i[69:75];
        row.lmt = i[75:81];
        row.l_sh = i[81:89];
        row.inv_lat = i[89:97];
        row.sza = i[97:106];
        
        rows.append(row);
    
    return rows;
