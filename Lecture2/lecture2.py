import matplotlib.pyplot as plt
from parser import *

# http://matplotlib.org/api/pyplot_api.html

def draw_one(x, y, params):
    __params = {
                "legend": ["y"],
                "xlabel": "x",
                "ylabel": "Y",
                "title": "Y(x)"
    };
    __params.update(params);
    plt.plot(x, y);
    plt.grid(True);
    plt.legend(__params["legend"]);
    plt.xlabel(__params["xlabel"]);
    plt.ylabel(__params["ylabel"]);
    plt.title(__params["title"]);
    plt.show();

def draw_two_at_one(x, y1, y2, params):
    __params = {
                "legend": ["y1", "y2"],
                "xlabel": "x",
                "ylabel": "Y",
                "title": "Y(x)"
    };
    __params.update(params);
    plt.plot(x, y1, "-", x, y2, "--");
    plt.grid(True);
    plt.legend(__params["legend"]);
    plt.xlabel(__params["xlabel"]);
    plt.ylabel(__params["ylabel"]);
    plt.title(__params["title"]);
    plt.show();

def draw_sub(x1, y1, params1, x2, y2, params2):
    __params = {
                "legend": ["y"],
                "xlabel": "x",
                "ylabel": "Y",
                "title": "Y(x)"
    };
    __params.update(params1);
    plt.subplot(2, 1, 1);
    plt.plot(x1, y1);
    plt.grid(True);
    plt.legend(__params["legend"]);
    plt.xlabel(__params["xlabel"]);
    plt.ylabel(__params["ylabel"]);
    plt.title(__params["title"]);
    
    __params = {
                "legend": ["y"],
                "xlabel": "x",
                "ylabel": "Y",
                "title": "Y(x)"
    };
    __params.update(params2);
    plt.subplot(2, 1, 2);
    plt.plot(x2, y2);
    plt.grid(True);
    plt.legend(__params["legend"]);
    plt.xlabel(__params["xlabel"]);
    plt.ylabel(__params["ylabel"]);
    plt.title(__params["title"]);
    plt.show();

if __name__ == '__main__':
    data = parser("1981217_de2_lang_500ms_v01.asc");
    ut = [i.ut for i in data];
    np = [i.np for i in data];
    temp = [i.temp for i in data];
    latitude = [i.latitude for i in data];
    longitude = [i.longitude for i in data];
    draw_one(ut, np, {
                      "legend": ["Density"], 
                      "xlabel": "UT, sec", 
                      "ylabel": "Density, cm^-3", 
                      "title": "Dynamic Explorer 2, density on neutral plasma component"
    });
    
    draw_two_at_one(ut, latitude, longitude, {
                      "legend": ["Latitude", "Longitude"], 
                      "xlabel": "UT, sec", 
                      "ylabel": "Latitude / Longitude, degree", 
                      "title": "Dynamic Explorer 2, geographic coordinates"
    });
    
    draw_sub(
             ut, np, {
                      "legend": ["Density"], 
                      "xlabel": "UT, sec", 
                      "ylabel": "Density, cm^-3", 
                      "title": "Dynamic Explorer 2, density on neutral plasma component"
    },
             latitude, longitude, {
                      "legend": ["Longitude"], 
                      "xlabel": "Longitude, degree", 
                      "ylabel": "Latitude, degree", 
                      "title": "Dynamic Explorer 2, geographic coordinates"
    });

    legend = [];
    
    plt.plot(ut, [i.orbit for i in data]); legend.append("orbit");
    plt.plot(ut, [i.year for i in data]); legend.append("year");
    plt.plot(ut, [i.day_of_year for i in data]); legend.append("day_of_year");
    plt.plot(ut, [i.temp  for i in data]); legend.append("temp");
    plt.plot(ut, [i.potential for i in data]); legend.append("potential");
    plt.plot(ut, [i.altitude for i in data]); legend.append("altitude");
    plt.plot(ut, [i.latitude for i in data]); legend.append("latitude");
    plt.plot(ut, [i.longitude for i in data]); legend.append("longitude");
    plt.plot(ut, [i.lst for i in data]); legend.append("lst");
    plt.plot(ut, [i.lmt for i in data]); legend.append("lmt");
    plt.plot(ut, [i.l_sh for i in data]); legend.append("l_sh");
    plt.plot(ut, [i.inv_lat for i in data]); legend.append("inv_lat");
    plt.plot(ut, [i.sza for i in data]); legend.append("sza");
    plt.legend(legend);
    plt.show();


