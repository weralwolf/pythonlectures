# http://www.rafekettler.com/magicmethods.html

class Vector(object):
    def __init__(self, a=0., b=0., c=0.):
        """Cartesian vector creation"""
        self.x = float(a);
        self.y = float(b);
        self.z = float(c);
    
    @property
    def length(self):
        """Length of cartesian vector """
        return (self.x ** 2 + self.y ** 2 + self.z ** 2) ** 0.5;
    
    @length.setter
    def length(self, value):
        """Easy method for length setting"""
        mult = value / self.length;
        self.x *= mult;
        self.y *= mult;
        self.z *= mult;
    
    @property
    def director(self):
        """Give same directed vector with length equals 1"""
        copy = Vector(self.x, self.y, self.z);
        copy.length = 1.;
        return copy;
    
    @staticmethod
    def null():
        """Null vector"""
        return Vector();
    
    @staticmethod
    def I():
        """One-vector creation"""
        return Vector(3.** -0.5, 3.** -0.5, 3.** -0.5);
    
    @property
    def tuple(self):
        """Translating vector into tuple of floats"""
        return (float(self.x), float(self.y), float(self.z));
    
    def __str__(self):
        """Defines behavior for when str() is called on an instance of your 
        class"""
        return "{%f, %f, %f}" % self.tuple;
    
    def __repr__(self):
        """Defines behavior for when repr() is called on an instance of your 
        class. The major difference between str() and repr() is intended 
        audience. repr() is intended to produce output that is mostly 
        machine-readable (in many cases, it could be valid Python code even), 
        whereas str() is intended to be human-readable"""
        return "<Vector {%f, %f, %f}>" % self.tuple;
    
    def __hash__(self):
        """Defines behavior for when hash() is called on an instance of your 
        class. It has to return an integer, and its result is used for quick key 
        comparison in dictionaries"""
        return hash(str(self));
    
    def __nonzero__(self):
        """Defines behavior for when bool() is called on an instance of your 
        class. Should return True or False, depending on whether you would want 
        to consider the instance to be True or False"""
        return self.x != 0 or self.y != 0 or self.z != 0;
    
    def __add__(self, other):
        """Implements addition"""
        if (isinstance(other, Vector)):
            return Vector(self.x + other.x, self.y + other.y, self.z + other.z);
        else:
            raise TypeError("Vector could not be added with non-vector");
    
    def __sub__(self, other):
        """Implements subtraction"""
        if (isinstance(other, Vector)):
            return Vector(self.x - other.x, self.y - other.y, self.z - other.z);
        else:
            raise TypeError("Vector could be subtracted with other vector");
    
    def __mul__(self, other):
        """Implements multiplication"""
        if (isinstance(other, (float, int, long, complex))):
            return Vector(self.x * other, self.y * other, self.z * other);
        elif (isinstance(other, Vector)):
            return self.x * other.x + self.y * other.y + self.z * other.z;
        else:
            raise TypeError("Vector could be multiplied with numeric or other vector");
    
    def __div__(self, other):
        """Implements division using the / operator"""
        if (isinstance(other, (float, int, long, complex))):
            return Vector(self.x / other, self.y / other, self.z / other);
        else:
            raise TypeError("Vector could be divided with numeric");
    
    def __neg__(self):
        """Unary minus sign"""
        return Vector(-self.x, -self.y, -self.z);
    
    def __iadd__(self, other):
        """Implements addition with assignment"""
        return self + other;

    def __isub__(self, other):
        """Implements subtraction with assignment"""
        return self - other;

    def __imul__(self, other):
        """Implements multiplication with assignment"""
        if (isinstance(other, (float, int, long, complex))):
            return self * other;
        else:
            raise TypeError("Vector could be multiplied with assignment only with numeric");

    def __idiv__(self, other):
        """Implements division with assignment using the /= operator"""
        if (isinstance(other, (float, int, long, complex))):
            return self / other;
        else:
            raise TypeError("Vector could be divided with assignment only with numeric");
