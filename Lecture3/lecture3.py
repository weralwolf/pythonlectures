class A:
    field_a = 0;
    dict = {};
    
    def __init__(self):
        self.b = [];
        
    def __repr__(self):
        return "<A| {field_a: %s}, {dict: %s}, {b: %s}>" % (
            str(self.field_a), str(self.dict), str(self.b)
        );

obj = A();
obj.b.append(12);
obj.b.append(3);
obj.field_a = 16;
obj.dict["hollo"] = "fox";

print "obj: ", obj;

jbo = A();
jbo.b.append(55);
jbo.b.append(66);
jbo.field_a = 6;
jbo.dict["jbo"] = "loves it";

print "jbo: ", jbo;
print "obj: ", obj;